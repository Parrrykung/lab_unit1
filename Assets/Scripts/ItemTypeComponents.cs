using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Paritud.gamedev3.ep1
{
    public class ItemTypeComponents : MonoBehaviour
    {
        
        [SerializeField]
        protected ItemType m_ItemType;

        public ItemType Type
        {
            get
            {
                return m_ItemType;
            }
            set
            {
                m_ItemType = value;
            }
        }

        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    } 
}
